import * as React from 'react'

import { Component, ComponentBag, NextComponentBag } from './Component'
import { shallowEqual } from './utils'

interface CounterState {
  readonly count: number
  readonly visible: boolean
}

const InnerComponent = ({ state, setState }: ComponentBag<CounterState>) => {
  const onIncrement = () =>
    setState(state => ({ count: state.count + 1, visible: state.visible }))

  const onDecrement = () =>
    setState(state => ({ count: state.count - 1, visible: state.visible }))

  const onNOP = () =>
    setState(state => ({ count: state.count, visible: state.visible }))

  return (
    <>
      <button className="button" onClick={onIncrement}>
        Increment
      </button>

      <p style={{ alignSelf: 'center' }} className="is-size-3">
        Count {state.count}
      </p>

      <button className="button" onClick={onDecrement}>
        Decrement
      </button>
      <button className="button" onClick={onNOP}>
        No Operation
      </button>
    </>
  )
}

type OuterState = { readonly visible: boolean }
type InnerState = OuterState & { readonly count: number }
export const ComponentCounter = () => (
  <Component
    initialState={{ visible: true }}
    component={({ state, setState }: ComponentBag<OuterState>) => {
      const onHide = () => setState(_ => ({ visible: false }))
      const onShow = () => setState(_ => ({ visible: true }))
      return (
        <div className="container">
          <div
            style={{
              display: 'flex',
              padding: '20px',
              alignSelf: 'center',
              flexDirection: 'column',
            }}
          >
            {state.visible ? (
              <>
                <button className="is-primary button" onClick={onHide}>
                  Hide
                </button>
                <Component
                  didMount={() => console.info('did mount')}
                  didUpdate={() => console.info('did update')}
                  shouldUpdate={({
                    state,
                    nextState,
                  }: NextComponentBag<InnerState>) => {
                    console.info('should update')
                    return !shallowEqual(state)(nextState)
                  }}
                  willUnmount={() => console.info('will unmount')}
                  initialState={{ count: 0, visible: true }}
                  component={InnerComponent}
                />
              </>
            ) : (
              <button className="is-primary button" onClick={onShow}>
                Show
              </button>
            )}
          </div>
        </div>
      )
    }}
  />
)
