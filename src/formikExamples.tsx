import * as React from 'react'
import { render } from 'react-dom'

export const Name = () => (
  <form>
    <label>
      Name
      <input type="text" />
    </label>
  </form>
)

export const ControlledName = ({ name }: any) => (
  <form>
    <label>
      Name
      <input type="text" name="name" value={name} />
    </label>
  </form>
)

export const NameView = ({
  values,
  touched,
  errors,
  handleChange,
  handleBlur,
  handleSubmit,
}: any) => {
  return (
    <form onSubmit={handleSubmit}>
      <label>
        Name
        <input
          type="text"
          name="firstName"
          value={values.firstName}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        {touched.firstName &&
          errors.firstName && (
            <div style={{ color: 'red' }}>{errors.firstName}</div>
          )}
      </label>
      <br />
      <label>
        Name
        <input
          type="text"
          name="lastName"
          value={values.lastName}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        {touched.lastName &&
          errors.lastName && (
            <div style={{ color: 'red' }}>{errors.lastName}</div>
          )}
      </label>
      <br />
      <input type="submit" value="submit" />
    </form>
  )
}
export class SmartForm extends React.Component<any, any> {
  readonly state: any = {
    values: this.props.initialValues,
    touched: {},
    errors: {},
  }

  readonly handleSubmit = (evt: any) => {
    evt.preventDefault()
    this.props.onSubmit(this.state.values)
  }
  readonly handleChange = (evt: any) => {
    this.setState({
      values: { ...this.state.values, [evt.target.name]: evt.target.value },
    })
    this.validate()
  }
  readonly validate = () => {
    this.setState({ errors: this.props.validator(this.state.values) })
  }
  readonly handleBlur = (evt: any) => {
    this.setState({
      touched: { ...this.state.touched, [evt.target.name]: true },
    })
    this.validate()
  }
  render(): JSX.Element {
    const Component = this.props.component
    return (
      <Component
        values={this.state.values}
        touched={this.state.touched}
        errors={this.state.errors}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        handleBlur={this.handleBlur}
      />
    )
  }
}

const nameValidator = (values: any) => {
  const errors = {
    firstName: values.firstName.length >= 10 || 'first name too small',
    lastName: values.lastName.length >= 5 || 'last name too small',
  }
  return errors
}

render(
  <SmartForm
    validator={nameValidator}
    initialValues={{ firstName: 'foo', lastName: 'bar' }}
    onSubmit={(values: any) => console.info(values)}
    component={NameView}
  />,
  document.getElementById('root'),
)
