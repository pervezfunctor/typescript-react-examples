import * as React from 'react'
import * as ReactDOM from 'react-dom'

export const e = React.createElement
export const render = ReactDOM.render
