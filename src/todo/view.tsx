import * as React from 'react'
import { Todo, TodoAction, TodoList, Filter, TodoState } from './reducer'
import { Formik, Field, Form, FormikProps } from 'formik'
import { connect } from 'react-redux'
import { Dispatch } from '../Reducer'

interface TodoItemProps {
  readonly todo: Todo
  onToggleTodo(id: number): void
  onDeleteTodo(id: number): void
}

interface TodoListProps {
  readonly todoList: ReadonlyArray<Todo>
  onToggleTodo(id: number): void
  onDeleteTodo(id: number): void
}

const TodoItem = ({ todo, onToggleTodo, onDeleteTodo }: TodoItemProps) => (
  <tr>
    <td>{todo.id}</td>
    <td>{todo.title}</td>
    <td>
      <input
        type="checkbox"
        checked={todo.done}
        onChange={_ => onToggleTodo(todo.id)}
      />
    </td>
    <td>
      <input
        type="button"
        onClick={_ => onDeleteTodo(todo.id)}
        value="Delete"
      />
    </td>
  </tr>
)

interface AddTodoProps {
  onAddTodo(values: string): void
}

type AddTodoInnerFormProps = { readonly title: string }

const AddTodoInnerForm = (props: FormikProps<AddTodoInnerFormProps>) => (
  <Form>
    <Field name="title" placeholder="Add Todo" />
    <div style={{ color: 'red' }}>
      {props.touched.title && props.errors.title}
    </div>
  </Form>
)

const AddTodoForm = (props: AddTodoProps) => (
  <Formik
    initialValues={{ title: '' }}
    onSubmit={(values: { readonly title: string }) =>
      props.onAddTodo(values.title)
    }
    component={AddTodoInnerForm}
  />
)

interface FilterProps {
  readonly filter: Filter
  onChangeFilter(filter: Filter): void
}

const Filter = (props: FilterProps) => (
  <div>
    <button onClick={_ => props.onChangeFilter('ALL')}>ALL</button>
    <button onClick={_ => props.onChangeFilter('COMPLETED')}>COMPLETED</button>
    <button onClick={_ => props.onChangeFilter('PENDING')}>PENDING</button>
  </div>
)

const dispatchers = (dispatch: Dispatch<TodoAction>) => ({
  onToggleTodo: (id: number) =>
    dispatch({ type: 'TOGGLE_TODO', payload: { id } }),

  onAddTodo: (title: string) =>
    dispatch({ type: 'ADD_TODO', payload: { title } }),

  onDeleteTodo: (id: number) =>
    dispatch({ type: 'DELETE_TODO', payload: { id } }),

  onChangeFilter: (filter: Filter) =>
    dispatch({ type: 'CHANGE_FILTER', payload: { filter } }),
})

const TodoListHeader = () => (
  <thead>
    <tr>
      <th>Id</th>
      <th>Title</th>
      <th>Done</th>
      <th />
    </tr>
  </thead>
)

const TodoList = (props: TodoListProps) =>
  props.todoList.length > 0 ? (
    <table className="table">
      <TodoListHeader />
      <tbody>
        {props.todoList.map(t => (
          <TodoItem
            key={t.id}
            todo={t}
            onToggleTodo={props.onToggleTodo}
            onDeleteTodo={props.onDeleteTodo}
          />
        ))}
      </tbody>
    </table>
  ) : (
    <h1>No todos</h1>
  )

const filteredTodos = (todoList: TodoList, filter: Filter) => {
  switch (filter) {
    case 'COMPLETED':
      return todoList.filter(t => t.done)
    case 'PENDING':
      return todoList.filter(t => !t.done)
    default:
      return todoList
  }
}

export const TodoApp = connect(
  (state: TodoState) => ({
    filter: state.filter,
    todoList: filteredTodos(state.todoList, state.filter),
  }),
  dispatchers,
)((props: any) => (
  <div>
    <AddTodoForm onAddTodo={props.onAddTodo} />
    <TodoList
      todoList={props.todoList}
      onToggleTodo={props.onToggleTodo}
      onDeleteTodo={props.onDeleteTodo}
    />
    <Filter filter={props.filter} onChangeFilter={props.onChangeFilter} />
  </div>
))
