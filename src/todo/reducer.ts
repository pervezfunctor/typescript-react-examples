export type Filter = 'ALL' | 'COMPLETED' | 'PENDING'

export type TodoAction =
  | {
      readonly type: 'ADD_TODO'
      readonly payload: { readonly title: string }
    }
  | {
      readonly type: 'TOGGLE_TODO'
      readonly payload: { readonly id: number }
    }
  | {
      readonly type: 'DELETE_TODO'
      readonly payload: { readonly id: number }
    }
  | {
      readonly type: 'CHANGE_FILTER'
      readonly payload: { readonly filter: Filter }
    }

export interface Todo {
  readonly id: number
  readonly title: string
  readonly done: boolean
}

export type TodoList = ReadonlyArray<Todo>

export interface TodoState {
  readonly filter: Filter
  readonly todoList: TodoList
}

const toggleTodo = (todoList: TodoList, id: number) =>
  todoList.map(todo => (todo.id !== id ? todo : { ...todo, done: !todo.done }))

const deleteTodo = (todoList: TodoList, id: number) =>
  todoList.filter(todo => todo.id !== id)

// tslint:disable:no-let
let nextID = 1000

const addTodo = (todoList: TodoList, title: string) => [
  ...todoList,
  { id: nextID++, title, done: false },
]

const todoList: ReadonlyArray<Todo> = [
  { id: 1, title: 'bring milk', done: false },
  { id: 2, title: 'take a walk', done: true },
]

const todoState = (todoList: TodoList, filter: Filter) => ({
  todoList,
  filter,
})

const initialTodoState: TodoState = { todoList, filter: 'ALL' }

export const todo = (
  state: TodoState = initialTodoState,
  action: TodoAction,
): TodoState => {
  const { todoList, filter } = state

  switch (action.type) {
    case 'ADD_TODO':
      return todoState(addTodo(todoList, action.payload.title), filter)
    case 'TOGGLE_TODO':
      return todoState(toggleTodo(todoList, action.payload.id), filter)
    case 'DELETE_TODO':
      return todoState(deleteTodo(todoList, action.payload.id), filter)
    case 'CHANGE_FILTER':
      return todoState(todoList, action.payload.filter)
    default:
      return state
  }
}
