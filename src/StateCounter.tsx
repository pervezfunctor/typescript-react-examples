import * as React from 'react'
import { State, StateComponentProps } from './State'
import { CounterView } from './CounterView'

type CounterComponentProps = StateComponentProps<{ readonly count: number }>

export const Counter = () => (
  <State
    initialValues={{ count: 0 }}
    component={({ state, setState }: CounterComponentProps) => (
      <CounterView
        count={state.count}
        onIncrement={() => setState({ count: state.count + 1 })}
        onDecrement={() => setState({ count: state.count - 1 })}
      />
    )}
  />
)
