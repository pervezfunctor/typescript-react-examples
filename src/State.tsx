import * as React from 'react'

export type StateComponent<P> = React.ComponentType<StateComponentProps<P>>

export interface StateComponentProps<S> {
  readonly state: S
  setState(state: Pick<S, keyof S>): void
}

interface StateProps<P> {
  readonly initialValues: P
  readonly component: StateComponent<P>
}

export class State<S> extends React.Component<StateProps<S>, S> {
  constructor(props: StateProps<S>) {
    super(props)
    // tslint:disable-next-line:no-object-mutation
    this.state = this.props.initialValues
  }
  render(): JSX.Element {
    const Component = this.props.component
    return (
      <Component state={this.state} setState={state => this.setState(state)} />
    )
  }
}
