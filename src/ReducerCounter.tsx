import * as React from 'react'

import { Reducer, ReducerComponentProps } from './Reducer'
import { CounterView } from './CounterView'

type CounterState = { readonly count: number }
type CounterAction = { readonly type: 'INCREMENT' | 'DECREMENT' }

const counterReducer = (state: CounterState, action: CounterAction) => {
  switch (action.type) {
    case 'INCREMENT':
      return { count: state.count + 1 }
    case 'DECREMENT':
      return { count: state.count - 1 }
    default:
      return state
  }
}

export const RCounter = () => (
  <Reducer
    initialValues={{ count: 1000 }}
    reducer={counterReducer}
    component={({
      state,
      dispatch,
    }: ReducerComponentProps<CounterState, CounterAction>) => (
      <CounterView
        count={state.count}
        onIncrement={() => dispatch({ type: 'INCREMENT' })}
        onDecrement={() => dispatch({ type: 'DECREMENT' })}
      />
    )}
  />
)
