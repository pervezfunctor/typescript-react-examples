import * as React from 'react'

export type Dispatch<A> = (action: A) => void

export interface ReducerComponentProps<S, A> {
  readonly state: S
  readonly dispatch: Dispatch<A>
}

export type ReducerComponent<S, A> = React.ComponentType<
  ReducerComponentProps<S, A>
>
interface ReducerProps<S, A> {
  readonly initialValues: S
  // tslint:disable-next-line:prefer-method-signature
  readonly reducer: (state: S, action: A) => S
  readonly component: ReducerComponent<S, A>
}
export class Reducer<S, A> extends React.Component<ReducerProps<S, A>, S> {
  readonly reducer: (state: S, action: A) => S
  constructor(props: ReducerProps<S, A>) {
    super(props)
    // tslint:disable:no-object-mutation
    this.state = this.props.initialValues
    this.reducer = this.props.reducer
    // tslint:enable:no-object-mutation
  }
  readonly dispatch = (action: A): void => {
    this.setState(state => this.reducer(state, action))
  }
  render(): JSX.Element {
    const Component = this.props.component
    return <Component state={this.state} dispatch={this.dispatch} />
  }
}
