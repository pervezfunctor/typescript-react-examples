import { join, map, pipe, keys, reduce } from 'utils'

interface Attributes {
  readonly [key: string]: string
}

interface Element {
  readonly tag: string | Function
  readonly attributes: Attributes
  readonly children: ReadonlyArray<Element | string>
}

// tslint:disable-next-line:readonly-array
type Children = Array<Element | string>

export const e = (
  tag: string | Function,
  attributes: Attributes,
  ...children: Children
): Element => ({
  tag,
  attributes,
  children,
})

const attributesToString = (attributes: Attributes): string =>
  pipe(
    keys(attributes),
    reduce((result, key) => `${result} ${key}='${attributes[key]}'`, ''),
  )

const elementToString = (element: Element | string): string => {
  if (typeof element === 'string') {
    return element
  }

  const attributes = attributesToString(element.attributes)
  const children = pipe(element.children, map(elementToString), join('\n'))

  if (typeof element.tag === 'string') {
    return `<${element.tag} ${attributes}>${children}</${element.tag}>`
  }
  return elementToString(
    element.tag({ ...element.attributes, children: element.children }),
  )
}

export const render = (dom: Element, root: HTMLElement): void => {
  // tslint:disable-next-line:no-object-mutation
  root.innerHTML = elementToString(dom)
}
