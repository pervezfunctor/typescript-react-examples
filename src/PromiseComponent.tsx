import * as React from 'react'

import { Component, ComponentBag, NextPropsComponentBag } from './Component'
import { pipe, set } from './utils'

// Create and use Either/Try?
interface PromiseComponentState<T> {
  readonly error?: Error
  readonly loading: boolean
  readonly data?: T
  readonly promise: Promise<T>
}

interface PromiseComponentProps<T> {
  readonly promise: Promise<T>
  readonly component?: React.ComponentType<PromiseComponentState<T>>
  readonly children?: React.ComponentType<PromiseComponentState<T>>
}

type Bag<T> = ComponentBag<PromiseComponentState<T>>

function fetchData<T>(promise: Promise<T>, { setState }: Bag<T>): void {
  const setData = (data: T) =>
    setState(state =>
      pipe(
        state,
        set('data', data),
        set('loading', false),
        set('error', undefined),
      ),
    )
  const setError = (error: Error) =>
    //  deliberately not setting data on error
    setState(state => pipe(state, set('error', error), set('loading', false)))

  // @TODO: maybe, I could implement then function which can be piped similar to map/set etc...
  setState(state => pipe(state, set('loading', true)))
  promise.then(setData).catch(setError)
}

export function PromiseComponent<T>(
  props: PromiseComponentProps<T>,
): JSX.Element {
  const Comp = props.component || props.children
  if (Comp === undefined) {
    throw new Error('no component or children provided to PromiseComponent')
  }
  return (
    <Component
      initialState={{
        promise: props.promise,
        loading: true,
      }}
      didMount={(bag: Bag<T>) => fetchData(props.promise, bag)}
      willReceiveProps={(
        bag: NextPropsComponentBag<PromiseComponentState<T>>,
      ) => {
        if (
          !bag.state.loading &&
          bag.nextProps.initialState.promise !== bag.props.initialState.promise
        ) {
          fetchData(props.promise, bag)
        }
      }}
    >
      {({ state }: Bag<T>) => (
        <Comp
          loading={state.loading}
          error={state.error}
          data={state.data}
          promise={props.promise}
        />
      )}
    </Component>
  )
}
