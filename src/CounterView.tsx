import * as React from 'react'

export interface CounterViewProps {
  readonly count: number
  onIncrement(): void
  onDecrement(): void
}

export const CounterView = ({
  count,
  onIncrement,
  onDecrement,
}: CounterViewProps) => (
  <>
    <div>Count: {count} </div>
    <button onClick={onIncrement}>+</button>
    <button onClick={onDecrement}>-</button>
  </>
)
