import * as React from 'react'
import * as ReactDOM from 'react-dom'

import 'bulma/css/bulma.css'

import { createStore } from 'redux'
import { Provider } from 'react-redux'

import { TodoApp } from './todo/view'
import { todo } from './todo/reducer'

const store = createStore(todo)

ReactDOM.render(
  <Provider store={store}>
    <TodoApp />
  </Provider>,
  document.getElementById('root'),
)

// import { Counter } from './counter/view'
// import { counter } from './counter/reducer'

// const store = createStore(counter)

// ReactDOM.render(
//   <Provider store={store}>
// <Counter />
// </Provider>,
//  document.getElementById('root'),
// )
