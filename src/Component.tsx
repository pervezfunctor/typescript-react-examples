import * as React from 'react'

export interface ComponentBag<S> {
  readonly state: S
  readonly props: ComponentValues<S>
  readonly context?: any

  setState(state: (prevState: S) => S, callback?: () => void): void
}

interface ComponentValues<S> {
  readonly initialState: S
  readonly component?: React.ComponentType<ComponentBag<S>>
  readonly children?: React.ComponentType<ComponentBag<S>>
}

interface ComponentLifecycle<S> {
  didMount?(bag: ComponentBag<S>): void
  willUnmount?(bag: ComponentBag<S>): void
  shouldUpdate?(bag: NextComponentBag<S>): boolean
  didUpdate?(bag: PrevComponentBag<S>): void
  willReceiveProps?(bag: NextPropsComponentBag<S>): void
}

type ComponentProps<S> = ComponentValues<S> & ComponentLifecycle<S>

export type NextPropsComponentBag<S> = ComponentBag<S> & {
  readonly nextProps: ComponentValues<S>
  readonly nextContext?: any
}

export type NextComponentBag<S> = NextPropsComponentBag<S> & {
  readonly nextState: S
}

export type PrevComponentBag<S> = ComponentBag<S> & {
  readonly prevState: S
  readonly prevProps: ComponentValues<S>
  readonly prevContext?: any
}

export class Component<S> extends React.Component<ComponentProps<S>, S> {
  readonly state: S = this.props.initialState

  readonly getBag = () => {
    return {
      state: this.state,
      props: this.props,
      context: this.context,
      setState: (fn: (state: S) => S, callback?: () => void): void =>
        this.setState(fn, callback),
    }
  }

  componentDidMount(): void {
    if (this.props.didMount) {
      this.props.didMount(this.getBag())
    }
  }

  shouldComponentUpdate(
    nextProps: ComponentProps<S>,
    nextState: S,
    nextContext: any,
  ): boolean {
    if (this.props.shouldUpdate) {
      return this.props.shouldUpdate({
        ...this.getBag(),
        nextProps,
        nextState,
        nextContext,
      })
    }
    return true
  }

  componentWillUnmount(): void {
    if (this.props.willUnmount) {
      this.props.willUnmount(this.getBag())
    }
  }

  componentDidUpdate(
    prevProps: ComponentProps<S>,
    prevState: S,
    prevContext: any,
  ): void {
    if (this.props.didUpdate) {
      this.props.didUpdate({
        ...this.getBag(),
        prevProps,
        prevState,
        prevContext,
      })
    }
  }

  // @TODO:  Use getDerivedStateFromProps when React 16.3 is released?
  componentWillReceiveProps?(
    nextProps: ComponentProps<S>,
    nextContext?: any,
  ): void {
    if (this.props.willReceiveProps) {
      this.props.willReceiveProps({
        ...this.getBag(),
        nextProps,
        nextContext,
      })
    }
  }

  render(): JSX.Element {
    const Component = this.props.component || this.props.children
    if (Component) {
      return <Component {...this.getBag()} />
    }
    throw new Error('no component or no children provided to Component')
  }
}
