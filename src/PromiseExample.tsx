import * as React from 'react'
import { Component, ComponentBag } from './Component'

import { PromiseComponent } from './PromiseComponent'
const delayedValue = (ms: number, value?: number): Promise<number> =>
  new Promise(resolve => setTimeout(() => resolve(value || Math.random()), ms))

// const value = (v: number) => Promise.resolve(v)
const error: Promise<number> = Promise.reject<number>(
  new Error('deliberate error'),
).catch(_ => {
  console.info('')
})

const innerPromise = delayedValue(1000)
const outerPromise = delayedValue(5000)

type HelloProps = { readonly value: number }

const Hello = (props: HelloProps) => {
  return (
    <Component initialState={{ value: props.value }}>
      {({ state: { value } }: ComponentBag<HelloProps>) => (
        <h1>Hello {value}</h1>
      )}
    </Component>
  )
}

export const PromiseExample = () => {
  return (
    <PromiseComponent
      promise={outerPromise}
      component={() => (
        <>
          <PromiseComponent
            promise={innerPromise}
            component={({ error, loading, data }) =>
              error ? (
                <h1>Error</h1>
              ) : loading ? (
                <h1>Loading...</h1>
              ) : (
                <Hello value={data ? data : 0} />
              )
            }
          />
          <PromiseComponent
            promise={error}
            component={({ error, loading, data }) =>
              error ? (
                <h1>Error</h1>
              ) : loading ? (
                <h1>Loading...</h1>
              ) : (
                <Hello value={data ? data : 0} />
              )
            }
          />
        </>
      )}
    />
  )
}
