import * as React from 'react'
import { connect } from 'react-redux'
import { CounterState, CounterAction } from './reducer'
import { Dispatch } from 'redux'

interface CounterProps {
  readonly count: number
  onIncrement(): void
  onDecrement(): void
}

const dispatchToProps = (dispatch: Dispatch<CounterAction>) => ({
  onIncrement: () => dispatch({ type: 'INCREMENT' }),
  onDecrement: () => dispatch({ type: 'DECREMENT' }),
})

const mapStateToProps = (state: CounterState): CounterState => state

const CounterView = (props: CounterProps) => (
  <div>
    <button onClick={props.onDecrement}>-</button>
    <span>Count: {props.count}</span>
    <button onClick={props.onIncrement}>+</button>
  </div>
)

export const Counter = connect(mapStateToProps, dispatchToProps)(CounterView)
