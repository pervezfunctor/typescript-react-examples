export type CounterState = { readonly count: number }

export type CounterAction =
  | { readonly type: 'INCREMENT' }
  | { readonly type: 'DECREMENT' }

const initialState: CounterState = { count: 1 }

export const counter = (
  state: CounterState = initialState,
  action: CounterAction,
) => {
  switch (action.type) {
    case 'INCREMENT':
      return { count: state.count + 1 }
    case 'DECREMENT':
      return { count: state.count - 1 }
    default:
      return state
  }
}
